PUNK - Primitives for Uncovering New Knowledge
===============================================

Working on developing algorithms and pipelines to automate Machine Learning:

- Data augmentation

- Feature selection

- Novelty detection

D3M
----
For submission and usage with respect to the broader D3M community see `D3M <d3m>`_.

Usage
------
For examples on how to use these primitives look at `examples <examples>`_.
